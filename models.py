from peewee import *

db = SqliteDatabase('./memes/database/conf.db')
''' for testing'''
# db = SqliteDatabase('conf.db')


class BaseModel(Model):
    class Meta:
        database = db


class ConfMember(BaseModel):
    name = CharField()
    user_id = IntegerField()
    to_notify_5050 = BooleanField(default=False)

    score_727 = IntegerField(default=0)


db.connect()
db.create_tables([ConfMember])
