# Kanfa Bot
I made this Telegram Bot for my friends group for fun!

## Commands List
1) А шанс (something) - returns random chance of something happening
2) А выбери (list of options) - returns one of the specified options, they must be separated by spaces, commas or "или" keyword
3) А кола (minutes) - notifies the user after set period of time
4) А кидай - sends a random meme from the memes folder
5) А мем - adds the sent meme to the memes folder (the command should be a caption to the meme)

## Management Commands List
1) /enable5050 - enables group notifications about KFC sales coupon 5050 (you must add coupon_api URL for this to work)
2) /disable5050 - disables group notifications about KFC sales coupon 5050 (you must add coupon_api URL for this to work)

## Usage

### Manual
Set the following environment variables:
``` shell
export API_TOKEN=<your TG bot api token, mandatory>
export COUPON_API=<coupon_api URL, optional>
```
Start the script:
```shell
python main.py
```

### Docker
After building the image, run it with the same environment variables set:
```shell
docker build -t kanfabot .
docker run -d --env=API_TOKEN=<TG bot api token> --env=COUPON_API=<coupon_api URL> kanfabot
```
