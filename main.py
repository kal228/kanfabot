import logging
import os

from telegram.ext import ApplicationBuilder, MessageHandler, filters, CommandHandler

from commands import (add_meme, chance, choice, who, create_timer_job, random_meme,
                      disable_5050, enable_5050, add_conf_member, show_727_leaderboard,
                      remove_conf_member, process_message)
import models

assert "API_TOKEN" in os.environ

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

if __name__ == '__main__':
    # Create bot application
    application = ApplicationBuilder().token(os.environ["API_TOKEN"]).build()

    # Define handlers for different text commands, text commands must start with base_regex
    base_regex = r'^([Аа]|[Сс][Оо][Нн][Яя])'
    add_meme_handler = MessageHandler(filters.CaptionRegex(rf'{base_regex} мем'), add_meme)
    chance_handler = MessageHandler(filters.Regex(rf'{base_regex} шанс'), chance)
    choice_handler = MessageHandler(filters.Regex(rf'{base_regex} выбери'), choice)
    who_handler = MessageHandler(filters.Regex(rf'{base_regex} кто'), who)
    timer_handler = MessageHandler(filters.Regex(rf'{base_regex} кола'), create_timer_job)
    random_meme_handler = MessageHandler(filters.Regex(rf'{base_regex} кидай'), random_meme)
    add_member_handler = MessageHandler(filters.StatusUpdate.NEW_CHAT_MEMBERS, add_conf_member)
    remove_member_handler = MessageHandler(filters.StatusUpdate.LEFT_CHAT_MEMBER, remove_conf_member)
    show_727_leaderboard_handler = CommandHandler('727', show_727_leaderboard)

    process_message_handler = MessageHandler(filters.ALL, process_message)


    # Define management commands (only accessible to group admins)
    enable_5050_handler = CommandHandler("enable5050", enable_5050)
    disable_5050_handler = CommandHandler("disable5050", disable_5050)

    # Add all the necessary handlers to the application
    application.add_handler(show_727_leaderboard_handler)
    application.add_handler(add_meme_handler)
    application.add_handler(chance_handler)
    application.add_handler(choice_handler)
    application.add_handler(who_handler)
    application.add_handler(timer_handler)
    application.add_handler(random_meme_handler)
    application.add_handler(add_member_handler)
    application.add_handler(remove_member_handler)

    application.add_handler(enable_5050_handler)
    application.add_handler(disable_5050_handler)

    application.add_handler(process_message_handler)

    # Start the application
    application.run_polling()
