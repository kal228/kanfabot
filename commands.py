import datetime
import os
import random
import uuid
import requests

from telegram import Update
from telegram.constants import ChatType, ChatMemberStatus
from telegram.ext import ContextTypes

from models import ConfMember

# user_id: (prev_message_datetime, suspicious_activity_counter, is banned)
user_activity = {}
suspicious_messages_ids = {}


def check_if_admin(func):
    async def wrapper(update, context):
        chat = update.effective_chat
        member = update.effective_user
        bot = context.bot

        member = await bot.get_chat_member(chat_id=chat.id, user_id=member.id)

        if (chat.type == ChatType.PRIVATE or
                member.status == ChatMemberStatus.ADMINISTRATOR or
                member.status == ChatMemberStatus.OWNER):
            return await func(update, context)
    return wrapper


# Add group member to the database if the record doesn't exist
def get_member(user):
    member = ConfMember.get_or_create(name=user.full_name, user_id=user.id)
    return member


memes_path = './memes'
memes = [file for file in os.listdir(memes_path) if os.path.isfile(os.path.join(memes_path, file))]


async def timer_callback(context: ContextTypes.DEFAULT_TYPE):
    job = context.job
    message_id = job.data

    percentage_chance = 0.01
    if random.random() < percentage_chance:
        picture = 'coke2'
    else:
        picture = 'coke1'
    with open('./memes/static/'+picture, "rb") as file:
        await context.bot.send_photo(chat_id=job.chat_id, photo=file, reply_to_message_id=message_id)


async def create_timer_job(update: Update, context: ContextTypes.DEFAULT_TYPE):
    chat_id = update.effective_message.chat_id
    message_id = update.effective_message.id
    
    responses = {
        "Aryan": "Treffen Sie mich in {} Minuten",
        "default": "Встретимся через {} минут",
        "reject": "Много хочешь",
    }

    response = ''
    should_reject = False

    try:
        due = float(update.message.text[7:].strip())

        match due:
            case 1488:
                response = responses['Aryan'].format(int(due))

            case _ if (due < 0 or due > 60*24):
                response = responses['reject']
                should_reject = True
            
            case _:
                response = responses['default'].format(int(due))

        await update.effective_message.reply_text(response)

        if (should_reject):
            return
        
        context.job_queue.run_once(timer_callback, due * 60, chat_id=chat_id, data=message_id)
    except (IndexError, ValueError):
        await update.effective_message.reply_text("Usage: а кола <минуты>")


# Sends the message that the user sent with a random chance at the end
async def chance(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user_message = update.effective_message.text.split()[2:]
    user_id = update.effective_user.id

    if user_message == ['7'] or user_message == ['72']:
        user_activity_data = user_activity.get(user_id)
        if user_activity_data is None:
            try_counter = 1
            user_activity[user_id] = (datetime.datetime.now(), try_counter)
        else:
            first_roll_datetime, try_counter = user_activity_data
            if datetime.datetime.now() - first_roll_datetime >= datetime.timedelta(hours=1):
                try_counter = 1
                first_roll_datetime = datetime.datetime.now()
            else:
                try_counter += 1
            if try_counter > 1:
                return
            user_activity[user_id] = (first_roll_datetime, try_counter)

    chance = random.randrange(0, 101 + 1)
    if (user_message == ['7'] and chance == 27) or (user_message == ['72'] and chance == 7):
        member, created = ConfMember.get_or_create(user_id=update.effective_user.id, name=update.effective_user.full_name)
        if try_counter == 1:
            member.score_727 += 3
        elif try_counter == 2:
            member.score_727 += 2
        elif try_counter == 3:
            member.score_727 += 1
        member.save()
    text = " ".join(user_message)
    await context.bot.send_message(chat_id=update.effective_chat.id,
                                   text=f"Шанс того, что {text}"
                                        f" равен {chance}%")


# Sends the user back one of the provided options
async def choice(update: Update, context: ContextTypes.DEFAULT_TYPE):
    text = " ".join(update.message.text.split()[2:])
    delimiters = [',', ' или ', ' or ', ' ']
    choices = []

    for delimiter in delimiters:
        if delimiter in text:
            choices = text.split(delimiter)
            break

    for i in range(len(choices)):
        choices[i] = choices[i].strip()

    choice = random.choice(choices)

    answer = random.choice([f"Я советую тебе {choice}", f"Думаю, стоит выбрать {choice}", f"Несомненно {choice}",
                            f"Если придётся выбирать, то уж лучше {choice}", f"Выбирай {choice} или лох"])
    await context.bot.send_message(chat_id=update.effective_chat.id, text=answer)


# Sends a random users name from the group
async def who(update: Update, context: ContextTypes.DEFAULT_TYPE):
    try:
        ids = []
        for member in ConfMember.select():
            ids.append(member.user_id)
        text = " ".join(update.message.text.split()[2:])
        user_id = random.choice(ids)
        chat_id = update.message.chat_id
        random_member = await context.bot.get_chat_member(chat_id, user_id)
        user_name = random_member.user.full_name
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                       text=f"Несомненно, {user_name} {text}")
    except IndexError:
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                       text=f"Произошла ошибка, проверьте список айдишников")


# Sends a random picture from the memes folder
async def random_meme(update: Update, context: ContextTypes.DEFAULT_TYPE):
    try:
        meme = random.choice(memes)
        with open('./memes/'+meme, "rb") as file:
            await context.bot.send_photo(chat_id=update.effective_chat.id, photo=file)
    except IndexError:
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                       text=f"мемов нет.")


# Adds the provided meme to the memes folder
async def add_meme(update: Update, context: ContextTypes.DEFAULT_TYPE):
    new_meme = await update.message.effective_attachment[-1].get_file()
    await new_meme.download_to_drive('./memes/'+str(uuid.uuid4()))
    await context.bot.send_message(chat_id=update.effective_chat.id, text=f"мем добавлен")


# TODO: fix the API
# Sends notifications to the group about new KFC sale coupon
async def send_5050(context: ContextTypes.DEFAULT_TYPE):
    try:
        r = requests.get(os.environ["COUPON_API"])
        r.raise_for_status()

        coupon = r.json()
        meal = coupon['meal']
        # price = coupon['price']

        chat_id = context.job.chat_id
        if meal is not None:
            await context.bot.send_message(chat_id=chat_id, text=f"Сегодня среда мои чюваки. Новый 5050 купон: {meal}")
    except requests.exceptions.HTTPError as error:
        print(error)


# Adds new group member to the database and greets them
async def add_conf_member(update: Update, context: ContextTypes.DEFAULT_TYPE):
    new_member = update.effective_message.new_chat_members[0]

    chat_id = update.effective_chat.id
    user_id = new_member.id
    user_name = new_member.full_name

    new_chat_member = ConfMember.create(name=user_name, user_id=user_id)
    new_chat_member.save()

    await context.bot.send_message(chat_id=chat_id, text=f"ну здарова {user_name}")


# Removes left group member from the database
async def remove_conf_member(update: Update, context: ContextTypes.DEFAULT_TYPE):
    left_member = update.effective_message.left_chat_member

    chat_id = update.effective_chat.id
    user_id = left_member.id
    user_name = left_member.full_name

    left_chat_member = ConfMember.get(user_id=user_id)
    left_chat_member.delete_instance()

    await context.bot.send_message(chat_id=chat_id, text=f"прощяй {user_name}")


async def show_727_leaderboard(update: Update, context: ContextTypes.DEFAULT_TYPE):
    leaderboard = "\n".join([f"{i+1}) {member.name}: {member.score_727}" for i, member in
                             enumerate(ConfMember.select().order_by(ConfMember.score_727.desc()))])
    leaderboard = "727 leaderboard:\n" + leaderboard
    await update.message.reply_text(text=leaderboard)


# def check_727():
#     now = datetime.datetime.now()
#     current_hour = now.hour
#     current_minute = now.minute
#     return current_minute == 27 and (current_hour == 7 or current_hour == 17 or current_hour == 19)


# Message processing function that applies after all the other callbacks, intended to process the message in some ways
async def process_message(update: Update, context: ContextTypes.DEFAULT_TYPE):
    # TODO: Add support for different groups to have different members
    # Only add users from groups to the database
    groups = ['group', 'supergroup']
    # correct_727_messages = ['727', '1727']
    if update.effective_chat.type in groups:
        get_member(update.effective_message.from_user)

    # TODO: implement bans
    # await context.bot.delete_message(chat_id=update.effective_chat.id, message_id=update.effective_message.id)

    # if update.message.text in correct_727_messages and check_727():
    #     member, _ = get_member(update.effective_message.from_user)
    #     member.score_727 += 1
    #     member.save()
    # elif update.message.text in correct_727_messages and not check_727():
    #     now = datetime.datetime.now()
    #     await update.message.reply_text(text=f"бро ты что дурак сейчас {now.hour}:{now.minute}")


# Enables the KFC sale coupon notifications in the group (admin only)
@check_if_admin
async def enable_5050(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if len(context.job_queue.get_jobs_by_name('5050 alert')) == 0:
        context.job_queue.run_repeating(callback=send_5050, first=1, interval=60*60*8,
                                        chat_id=update.effective_message.chat_id, name='5050 alert',
                                        data=update.effective_chat.id)
        await context.bot.send_message(chat_id=update.effective_message.chat_id, text="5050 alerts enabled")


# Disables the KFC sale coupon notifications in the group (admin only)
@check_if_admin
async def disable_5050(update: Update, context: ContextTypes.DEFAULT_TYPE):
    for job in context.job_queue.get_jobs_by_name('5050 alert'):
        job.schedule_removal()
    await context.bot.send_message(chat_id=update.effective_message.chat_id, text="5050 alerts disabled")

